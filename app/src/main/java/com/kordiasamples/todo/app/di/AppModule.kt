package com.kordiasamples.todo.app.di

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kordiasamples.todo.data.db.TodoDatabase
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory = factory

    @Provides
    fun provideTodoDatabase(context: Application): TodoDatabase =
        Room.databaseBuilder(context, TodoDatabase::class.java, "todo_database")
            .build()
}
