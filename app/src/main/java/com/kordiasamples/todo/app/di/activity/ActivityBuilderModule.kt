package com.kordiasamples.todo.app.di.activity

import com.kordiasamples.todo.app.di.activity.login.LoginActivityModule
import com.kordiasamples.todo.app.di.activity.login.LoginActivityViewModelModule
import com.kordiasamples.todo.app.di.activity.login.LoginActivityScope
import com.kordiasamples.todo.app.di.activity.main.MainActivityModule
import com.kordiasamples.todo.app.di.activity.main.MainActivityViewModelModule
import com.kordiasamples.todo.app.di.activity.main.MainActivityScope
import com.kordiasamples.todo.app.di.activity.main.fragment.MainActivityFragmentsModule
import com.kordiasamples.todo.presentation.activity.login.LoginActivity
import com.kordiasamples.todo.presentation.activity.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    // start scoping activities here
    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginActivityViewModelModule::class, LoginActivityModule::class])
    abstract fun contributeLoginActivity(): LoginActivity?

    @MainActivityScope
    @ContributesAndroidInjector(modules = [MainActivityFragmentsModule::class, MainActivityViewModelModule::class, MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity?
}
