package com.kordiasamples.todo.app

import android.content.Context
import androidx.multidex.MultiDex
import com.kordiasamples.todo.app.di.DaggerAppComponent
import com.kordiasamples.todo.presentation.DarkModeHelper
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {
    override fun onCreate() {
        super.onCreate()
        DarkModeHelper(baseContext).applyDayNightMode()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication?>? =
        DaggerAppComponent.factory().create(this)

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}