package com.kordiasamples.todo.app.di.activity.main.fragment.task

import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.app.di.ViewModelKey
import com.kordiasamples.todo.presentation.activity.main.fragment.task.TaskFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TaskFragmentViewModelModule {

    @Binds
    @IntoMap
    @TaskFragmentScope
    @ViewModelKey(TaskFragmentViewModel::class)
    abstract fun bindTaskFragmentViewModel(taskFragment: TaskFragmentViewModel): ViewModel
}
