package com.kordiasamples.todo.app.di.activity.main

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class MainActivityScope
