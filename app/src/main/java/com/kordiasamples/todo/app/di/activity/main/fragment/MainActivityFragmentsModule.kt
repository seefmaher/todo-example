package com.kordiasamples.todo.app.di.activity.main.fragment

import com.kordiasamples.todo.app.di.activity.main.fragment.home.HomeFragmentModule
import com.kordiasamples.todo.app.di.activity.main.fragment.home.HomeFragmentScope
import com.kordiasamples.todo.app.di.activity.main.fragment.home.HomeFragmentViewModelModule
import com.kordiasamples.todo.app.di.activity.main.fragment.settings.SettingsFragmentModule
import com.kordiasamples.todo.app.di.activity.main.fragment.settings.SettingsFragmentViewModelModule
import com.kordiasamples.todo.app.di.activity.main.fragment.settings.SettingsFragmentScope
import com.kordiasamples.todo.app.di.activity.main.fragment.task.TaskFragmentModule
import com.kordiasamples.todo.app.di.activity.main.fragment.task.TaskFragmentViewModelModule
import com.kordiasamples.todo.app.di.activity.main.fragment.task.TaskFragmentScope
import com.kordiasamples.todo.app.di.activity.main.fragment.test.TestFragmentModule
import com.kordiasamples.todo.app.di.activity.main.fragment.test.TestFragmentScope
import com.kordiasamples.todo.app.di.activity.main.fragment.test.TestFragmentViewModelModule
import com.kordiasamples.todo.presentation.activity.main.fragment.test.TestFragment
import com.kordiasamples.todo.presentation.activity.main.fragment.home.HomeFragment
import com.kordiasamples.todo.presentation.activity.main.fragment.settings.SettingsFragment
import com.kordiasamples.todo.presentation.activity.main.fragment.task.TaskFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityFragmentsModule {

    // start scoping fragments here
    @HomeFragmentScope
    @ContributesAndroidInjector(modules = [HomeFragmentViewModelModule::class, HomeFragmentModule::class])
    abstract fun homeFragment(): HomeFragment

    @SettingsFragmentScope
    @ContributesAndroidInjector(modules = [SettingsFragmentViewModelModule::class, SettingsFragmentModule::class])
    abstract fun settingsFragment(): SettingsFragment

    @TaskFragmentScope
    @ContributesAndroidInjector(modules = [TaskFragmentViewModelModule::class, TaskFragmentModule::class])
    abstract fun taskFragment(): TaskFragment

    @TestFragmentScope
    @ContributesAndroidInjector(modules = [TestFragmentViewModelModule::class, TestFragmentModule::class])
    abstract fun testFragment(): TestFragment
}