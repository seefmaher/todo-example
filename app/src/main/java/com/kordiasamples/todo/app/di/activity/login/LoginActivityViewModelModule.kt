package com.kordiasamples.todo.app.di.activity.login

import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.app.di.ViewModelKey
import com.kordiasamples.todo.presentation.activity.login.LoginActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class LoginActivityViewModelModule {

    // this module is shared between the activity and its fragments and view models
    @Binds
    @IntoMap
    @LoginActivityScope
    @ViewModelKey(LoginActivityViewModel::class)
    internal abstract fun bindLoginActivityViewModel(loginActivity: LoginActivityViewModel): ViewModel

}