package com.kordiasamples.todo.app.di.activity.main.fragment.task

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class TaskFragmentScope
