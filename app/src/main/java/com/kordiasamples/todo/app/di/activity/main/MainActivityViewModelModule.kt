package com.kordiasamples.todo.app.di.activity.main

import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.app.di.ViewModelKey
import com.kordiasamples.todo.presentation.activity.main.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainActivityViewModelModule {

    // this module is shared between the activity and its fragments and view models
    @Binds
    @IntoMap
    @MainActivityScope
    @ViewModelKey(MainActivityViewModel::class)
    internal abstract fun bindMainActivityViewModel(mainActivity: MainActivityViewModel): ViewModel
}