package com.kordiasamples.todo.app.di.activity.main.fragment.test

import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.app.di.ViewModelKey
import com.kordiasamples.todo.presentation.activity.main.fragment.test.TestFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TestFragmentViewModelModule {

    @Binds
    @IntoMap
    @TestFragmentScope
    @ViewModelKey(TestFragmentViewModel::class)
    abstract fun bindTestFragmentViewModel(testFragment: TestFragmentViewModel): ViewModel
}
