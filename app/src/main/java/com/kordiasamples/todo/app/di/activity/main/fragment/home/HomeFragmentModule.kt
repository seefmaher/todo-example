package com.kordiasamples.todo.app.di.activity.main.fragment.home

import com.kordiasamples.todo.data.db.TodoDatabase
import com.kordiasamples.todo.data.db.task.TaskDao
import com.kordiasamples.todo.data.repository.TaskRepositoryImpl
import com.kordiasamples.todo.domain.repository.TaskRepository
import com.kordiasamples.todo.domain.usecase.task.TaskUsecase
import com.kordiasamples.todo.domain.usecase.task.TaskUsecaseImpl
import dagger.Module
import dagger.Provides

@Module
class HomeFragmentModule {

    @Provides
    @HomeFragmentScope
    fun provideTaskDao(todoDatabase: TodoDatabase): TaskDao = todoDatabase.taskDao()


    @Provides
    @HomeFragmentScope
    fun provideTaskRepository(taskDao: TaskDao): TaskRepository =
        TaskRepositoryImpl(taskDao)

    @Provides
    @HomeFragmentScope
    fun provideTaskUsecase(taskRepository: TaskRepository): TaskUsecase =
        TaskUsecaseImpl(taskRepository)

//    @Provides
//    @HomeFragmentScope
//    @Named("homestr")
//    fun str(): String = "Home Scope Str"
}
