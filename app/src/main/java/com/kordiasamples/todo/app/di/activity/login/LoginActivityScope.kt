package com.kordiasamples.todo.app.di.activity.login

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class LoginActivityScope
