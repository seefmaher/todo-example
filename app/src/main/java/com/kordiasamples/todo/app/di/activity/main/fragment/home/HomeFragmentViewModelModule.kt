package com.kordiasamples.todo.app.di.activity.main.fragment.home

import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.app.di.ViewModelKey
import com.kordiasamples.todo.presentation.activity.main.fragment.home.HomeFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class HomeFragmentViewModelModule {

    @Binds
    @IntoMap
    @HomeFragmentScope
    @ViewModelKey(HomeFragmentViewModel::class)
    abstract fun bindHomeFragmentViewModel(homeFragmentViewModel: HomeFragmentViewModel): ViewModel
}