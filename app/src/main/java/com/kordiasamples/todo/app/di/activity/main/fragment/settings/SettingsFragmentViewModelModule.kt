package com.kordiasamples.todo.app.di.activity.main.fragment.settings

import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.app.di.ViewModelKey
import com.kordiasamples.todo.presentation.activity.main.fragment.settings.SettingsFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SettingsFragmentViewModelModule {

    @Binds
    @IntoMap
    @SettingsFragmentScope
    @ViewModelKey(SettingsFragmentViewModel::class)
    abstract fun bindSettingsFragmentViewModel(settingsFragment: SettingsFragmentViewModel): ViewModel

//    @Provides
////    @SettingsFragmentScope
//    @Named("settingsstr")
//    fun str(): String = "Settings Scope Str"
}
