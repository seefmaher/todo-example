package com.kordiasamples.todo.domain.usecase.task

import com.kordiasamples.todo.domain.model.TaskDTO
import com.kordiasamples.todo.domain.repository.TaskRepository
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class TaskUsecaseImpl @Inject constructor(var taskRepository: TaskRepository) : TaskUsecase {

    override fun insertTask(task: TaskDTO) {
        taskRepository.insertTask(task)
    }

    override fun getAllTasks(): Flowable<List<TaskDTO>> {
        return taskRepository.getAllTasks()
    }

    override fun getTask(id: Long): Single<TaskDTO> {
        return taskRepository.getTask(id)!!
    }

    override fun insertCall() {
        taskRepository.insertCall()
    }
}