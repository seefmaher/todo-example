package com.kordiasamples.todo.domain

enum class SharedValueFlags {
    STRING_DB_NAME, STRING_BASE_URL, STRING_USERNAME, STRING_PASSWORD, INT_DARK_MODE
}

