package com.kordiasamples.todo.domain.repository

import com.kordiasamples.todo.domain.model.TaskDTO
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

interface TaskRepository {

    fun insertTask(task: TaskDTO)

    fun getAllTasks(): Flowable<List<TaskDTO>>

    fun getTask(id: Long): Single<TaskDTO>?

    fun insertCall()

}