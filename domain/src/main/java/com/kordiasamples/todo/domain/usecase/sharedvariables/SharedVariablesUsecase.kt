package com.kordiasamples.todo.domain.usecase.sharedvariables

import com.kordiasamples.todo.domain.SharedValueFlags

interface SharedVariablesUsecase {
    fun getStringSharedVariable(flag: SharedValueFlags?): String?
    fun setStringSharedVariable(flag: SharedValueFlags?, value: String?)
    fun setIntSharedVariable(flag: SharedValueFlags?, value: Int)
    fun getIntSharedVariable(flag: SharedValueFlags?): Int
    fun setDoubleSharedVariable(flag: SharedValueFlags?, value: Double)
    fun getDoubleSharedVariable(flag: SharedValueFlags?): Double
    fun setLongSharedVariable(flag: SharedValueFlags?, value: Long)
    fun getLongSharedVariable(flag: SharedValueFlags?): Long
    fun setBooleanSharedVariable(flag: SharedValueFlags?, value: Boolean)
    fun getBooleanSharedVariable(flag: SharedValueFlags?): Boolean
}
