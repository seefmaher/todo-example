package com.kordiasamples.todo.domain.usecase.sharedvariables

import com.kordiasamples.todo.domain.SharedValueFlags
import com.kordiasamples.todo.domain.repository.SharedVariablesRepository
import com.kordiasamples.todo.domain.usecase.sharedvariables.SharedVariablesUsecase

class SharedVariablesUsecaseImpl constructor(
    var sharedVariablesRepository: SharedVariablesRepository
) : SharedVariablesUsecase {

    override fun getStringSharedVariable(flag: SharedValueFlags?): String? {
        return sharedVariablesRepository.getStringSharedVariable(flag)
    }

    override fun setStringSharedVariable(flag: SharedValueFlags?, value: String?) {
        sharedVariablesRepository.setStringSharedVariable(flag, value)
    }

    override fun setIntSharedVariable(flag: SharedValueFlags?, value: Int) {
        sharedVariablesRepository.setIntSharedVariable(flag, value)
    }

    override fun getIntSharedVariable(flag: SharedValueFlags?): Int {
        return sharedVariablesRepository.getIntSharedVariable(flag)
    }

    override fun setDoubleSharedVariable(flag: SharedValueFlags?, value: Double) {
        sharedVariablesRepository.setDoubleSharedVariable(flag, value)
    }

    override fun getDoubleSharedVariable(flag: SharedValueFlags?): Double {
        return sharedVariablesRepository.getDoubleSharedVariable(flag)
    }

    override fun setLongSharedVariable(flag: SharedValueFlags?, value: Long) {
        sharedVariablesRepository.setLongSharedVariable(flag, value)
    }

    override fun getLongSharedVariable(flag: SharedValueFlags?): Long {
        return sharedVariablesRepository.getLongSharedVariable(flag)
    }

    override fun setBooleanSharedVariable(flag: SharedValueFlags?, value: Boolean) {
        sharedVariablesRepository.setBooleanSharedVariable(flag, value)
    }

    override fun getBooleanSharedVariable(flag: SharedValueFlags?): Boolean {
        return sharedVariablesRepository.getBooleanSharedVariable(flag)
    }
}