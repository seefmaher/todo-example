package com.kordiasamples.todo.domain.model

data class TaskDTO(val id: Long, val taskTitle: String, val taskDescription: String, val taskCompleted: Boolean) {
}