package com.kordiasamples.todo.data.repository

import com.kordiasamples.todo.data.SharedVariables
import com.kordiasamples.todo.domain.SharedValueFlags
import com.kordiasamples.todo.domain.repository.SharedVariablesRepository
import javax.inject.Inject

class SharedVariablesRepositoryImpl constructor(var sharedVariables: SharedVariables) :
    SharedVariablesRepository {
    // = sharedVariables
    override fun setStringSharedVariable(flag: SharedValueFlags?, value: String?) {
        sharedVariables.setStringSharedVariable(flag, value)
    }

    override fun getStringSharedVariable(flag: SharedValueFlags?): String? {
        return sharedVariables.getStringSharedVariable(flag)
    }

    override fun setIntSharedVariable(flag: SharedValueFlags?, value: Int) {
        sharedVariables.setIntSharedVariable(flag, value)
    }

    override fun getIntSharedVariable(flag: SharedValueFlags?): Int {
        return sharedVariables.getIntSharedVariable(flag)
    }

    override fun setDoubleSharedVariable(flag: SharedValueFlags?, value: Double) {
        sharedVariables.setDoubleSharedVariable(flag, value)
    }

    override fun getDoubleSharedVariable(flag: SharedValueFlags?): Double {
        return sharedVariables.getDoubleSharedVariable(flag)
    }

    override fun setLongSharedVariable(flag: SharedValueFlags?, value: Long) {
        sharedVariables.setLongSharedVariable(flag, value)
    }

    override fun getLongSharedVariable(flag: SharedValueFlags?): Long {
        return sharedVariables.getLongSharedVariable(flag)
    }

    override fun setBooleanSharedVariable(flag: SharedValueFlags?, value: Boolean) {
        sharedVariables.setBooleanSharedVariable(flag, value)
    }

    override fun getBooleanSharedVariable(flag: SharedValueFlags?): Boolean {
        return sharedVariables.getBooleanSharedVariable(flag)
    }

}
