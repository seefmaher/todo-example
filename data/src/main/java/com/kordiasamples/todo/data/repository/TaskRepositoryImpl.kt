package com.kordiasamples.todo.data.repository

import android.util.Log
import com.kordiasamples.todo.data.db.task.TaskDao
import com.kordiasamples.todo.data.mapper.map
import com.kordiasamples.todo.domain.model.TaskDTO
import com.kordiasamples.todo.domain.repository.TaskRepository
import hu.akarnokd.rxjava3.bridge.RxJavaBridge
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.Executors
import javax.inject.Inject

class TaskRepositoryImpl @Inject constructor(var taskDao: TaskDao) : TaskRepository {

    override fun insertTask(task: TaskDTO) {

//        Executors.newSingleThreadExecutor().execute {
            taskDao.insertTask(task.map())
//        }
    }

    override fun getAllTasks(): Flowable<List<TaskDTO>> {

        return RxJavaBridge.toV3Flowable(taskDao.getAllTasks().map{ list -> list.map { it.map() } })

    }

    override fun getTask(id: Long): Single<TaskDTO>? {
        return RxJavaBridge.toV3Single(taskDao.getTask(id).map { it.map() })
    }

    override fun insertCall() {
        Log.d("insertcall", "Insert call received")
    }
}