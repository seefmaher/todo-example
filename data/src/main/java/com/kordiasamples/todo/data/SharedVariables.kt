package com.kordiasamples.todo.data

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.preference.PreferenceManager
import com.kordiasamples.todo.domain.SharedValueFlags
import java.util.concurrent.Semaphore

@SuppressLint("CommitPrefEdits")
class SharedVariables private constructor(mContext: Context) {
    private val mSharedPreference: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
    private val mSharedPreferenceEditor: Editor
    private val mSemaphore: Semaphore
    fun setStringSharedVariable(flag: SharedValueFlags?, value: String?) {
        try {
            mSemaphore.acquire()
            mSharedPreferenceEditor.putString(java.lang.String.valueOf(flag), value)
            mSharedPreferenceEditor.commit()
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    fun getStringSharedVariable(flag: SharedValueFlags?): String? {
        var returnValue: String? = SPF_STRING_NO_VALUE_FOUND
        try {
            mSemaphore.acquire()
            returnValue = mSharedPreference.getString(
                java.lang.String.valueOf(flag),
                SPF_STRING_NO_VALUE_FOUND
            )
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return returnValue
    }

    fun setIntSharedVariable(flag: SharedValueFlags?, value: Int) {
        try {
            mSemaphore.acquire()
            mSharedPreferenceEditor.putInt(java.lang.String.valueOf(flag), value)
            mSharedPreferenceEditor.commit()
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    fun getIntSharedVariable(flag: SharedValueFlags?): Int {
        var returnValue = SPF_INT_NO_VALUE_FOUND
        try {
            mSemaphore.acquire()
            returnValue = mSharedPreference.getInt(
                java.lang.String.valueOf(flag),
                SPF_INT_NO_VALUE_FOUND
            )
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return returnValue
    }

    fun setDoubleSharedVariable(flag: SharedValueFlags?, value: Double) {
        try {
            mSemaphore.acquire()
            mSharedPreferenceEditor.putLong(
                java.lang.String.valueOf(flag),
                java.lang.Double.doubleToLongBits(value)
            )
            mSharedPreferenceEditor.commit()
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    fun getDoubleSharedVariable(flag: SharedValueFlags?): Double {
        var returnValue = SPF_INT_NO_VALUE_FOUND.toDouble()
        try {
            mSemaphore.acquire()
            returnValue = java.lang.Double.longBitsToDouble(
                mSharedPreference.getLong(
                    java.lang.String.valueOf(flag),
                    SPF_INT_NO_VALUE_FOUND.toLong()
                )
            )
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return returnValue
    }

    fun getLongSharedVariable(flag: SharedValueFlags?): Long {
        var returnValue = SPF_LONG_NO_VALUE_FOUND.toLong()
        try {
            mSemaphore.acquire()
            returnValue = mSharedPreference.getLong(
                java.lang.String.valueOf(flag),
                SPF_LONG_NO_VALUE_FOUND.toLong()
            )
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return returnValue
    }

    fun setLongSharedVariable(flag: SharedValueFlags?, value: Long) {
        try {
            mSemaphore.acquire()
            mSharedPreferenceEditor.putLong(java.lang.String.valueOf(flag), value)
            mSharedPreferenceEditor.commit()
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    fun setBooleanSharedVariable(flag: SharedValueFlags?, value: Boolean) {
        try {
            mSemaphore.acquire()
            mSharedPreferenceEditor.putBoolean(java.lang.String.valueOf(flag), value)
            mSharedPreferenceEditor.commit()
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    fun getBooleanSharedVariable(flag: SharedValueFlags?): Boolean {
        var returnValue = SPF_BOOLEAN_NO_VALUE_FOUND
        try {
            mSemaphore.acquire()
            returnValue = mSharedPreference.getBoolean(
                java.lang.String.valueOf(flag),
                SPF_BOOLEAN_NO_VALUE_FOUND
            )
            mSemaphore.release()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return returnValue
    }

    companion object {
        private const val SPF_STRING_NO_VALUE_FOUND = "NULL"
        private const val SPF_INT_NO_VALUE_FOUND = -1
        private const val SPF_LONG_NO_VALUE_FOUND = -1
        private const val SPF_BOOLEAN_NO_VALUE_FOUND = false
        private var mSharedVariables: SharedVariables? = null
        fun getInstance(context: Context): SharedVariables? {
            if (mSharedVariables == null) {
                mSharedVariables = SharedVariables(context)
            }
            return mSharedVariables
        }
    }
init {
        mSharedPreferenceEditor = mSharedPreference.edit()
        mSemaphore = Semaphore(
            Constants.MAX_PROCESS_AVAILABLE,
            true
        )
    }
}
