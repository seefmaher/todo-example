package com.kordiasamples.todo.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kordiasamples.todo.data.db.task.TaskDao
import com.kordiasamples.todo.data.db.task.TaskData

/**
 * Created by Dr.jacky on 9/14/2018.
 */
/**
 * Database class with all of its dao classes
 */
@Database(entities = [TaskData::class], version = 1, exportSchema = false)
abstract class TodoDatabase : RoomDatabase() {

    abstract fun taskDao(): TaskDao
}