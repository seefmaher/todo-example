package com.kordiasamples.todo.data

import com.kordiasamples.todo.domain.SharedValueFlags

object Constants {
    fun getBaseUrl(sharedVariables: SharedVariables): String? {
        return sharedVariables.getStringSharedVariable(SharedValueFlags.STRING_BASE_URL)
    }

    const val MAX_PROCESS_AVAILABLE = 1
}
