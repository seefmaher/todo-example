package com.kordiasamples.todo.data.mapper

import com.kordiasamples.todo.data.db.task.TaskData
import com.kordiasamples.todo.domain.model.TaskDTO

fun TaskData.map() = TaskDTO(
    id = id,
    taskTitle = taskTitle,
    taskDescription = taskDescription,
    taskCompleted = taskCompleted
)

fun TaskDTO.map() = TaskData(
    id = id,
    taskTitle = taskTitle,
    taskDescription = taskDescription,
    taskCompleted = taskCompleted
)