package com.kordiasamples.todo.data.db.task

import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface TaskDao {

    @Insert
    fun insertTask(task: TaskData)

    @Update
    fun updateTask(task: TaskData)

    @Delete
    fun deleteTask(task: TaskData)

    @Query("SELECT * FROM task_table")
    fun getAllTasks(): Flowable<List<TaskData>>

    @Query("SELECT * FROM task_table WHERE id = :id")
    fun getTask(id: Long): Single<TaskData>
}