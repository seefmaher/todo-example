package com.kordiasamples.todo.presentation.activity.main.fragment.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.kordiasamples.todo.presentation.R
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class TestFragment : DaggerFragment() {

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    val viewModel: TestFragmentViewModel by lazy {
        ViewModelProvider(this, provider).get(TestFragmentViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.showLog()
        return inflater.inflate(R.layout.fragment_test, container, false)
    }
}