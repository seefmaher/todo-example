package com.kordiasamples.todo.presentation.activity.main

import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.kordiasamples.todo.presentation.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener
    , NavController.OnDestinationChangedListener {

    lateinit var navigationView: NavigationView
    lateinit var navController: NavController
    lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        navigationView = nav_view
        drawerLayout = drawer_layout
        initiateNavigation()
        title = "Todo Sample"
    }

    private fun initiateNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        NavigationUI.setupWithNavController(navigationView, navController)
        navigationView.setNavigationItemSelectedListener(this)
        navController.addOnDestinationChangedListener(this)
    }

    private fun isValidDestination(destination: Int): Boolean =
        destination != navController.currentDestination?.id

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.nav_settings ->
                if (isValidDestination(R.id.settingsFragment))
                    navController.navigate(R.id.action_mainFragment_to_settingsFragment)

            R.id.nav_test ->
                if (isValidDestination(R.id.testFragment))
                    navController.navigate(R.id.action_mainFragment_to_testFragment)

            else ->
                navController.navigate(R.id.action_anywhere_to_mainFragment)

        }

        item.isChecked = true
        drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onSupportNavigateUp(): Boolean =
        NavigationUI.navigateUp(navController, drawerLayout)


    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        val enabled = destination.id == R.id.mainFragment
        val lockMode =
            if (enabled) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED
        drawerLayout.setDrawerLockMode(lockMode)
    }
}
