package com.kordiasamples.todo.presentation.activity.main.fragment.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.kordiasamples.todo.domain.model.TaskDTO
import com.kordiasamples.todo.presentation.R
import com.kordiasamples.todo.presentation.activity.main.fragment.task.TaskFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : DaggerFragment(), View.OnClickListener {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val viewModel: HomeFragmentViewModel by lazy {
        ViewModelProvider(this, factory).get(HomeFragmentViewModel::class.java)
    }


    private lateinit var navController: NavController

//    private lateinit

    private val adapter: HomeFragmentTaskAdapter by lazy {
        //var tasksList: List<TaskDTO> = listOf()

        viewModel.getAllTasks().observe(viewLifecycleOwner, Observer {
//            it.forEach{task -> Log.d("datachanged", task.taskTitle)}
//            tasksList = it
            adapter.updateDataSet(it)
            Log.d("taskscount", it.size.toString())
        })
        HomeFragmentTaskAdapter(listOf())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener(this)
        navController = Navigation.findNavController(view)
        main_recycler_view.adapter = adapter
        main_recycler_view.layoutManager = LinearLayoutManager(requireContext())


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            fab.id -> {
                viewModel.addTask()
//                navController.navigate(R.id.action_mainFragment_to_taskFragment)
//                var intent = Intent(context,TaskFragment::class.java)
//                startActivity(intent)
            }
        }
    }
}
