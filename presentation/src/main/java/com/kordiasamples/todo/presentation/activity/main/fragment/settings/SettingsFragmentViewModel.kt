package com.kordiasamples.todo.presentation.activity.main.fragment.settings

import android.app.Application
import androidx.lifecycle.ViewModel
import com.kordiasamples.todo.presentation.DarkModeHelper
import javax.inject.Inject

class SettingsFragmentViewModel @Inject constructor(application: Application) : ViewModel() {

//    @Inject
//    @Named("settingsstr")
//    lateinit var str: String

    private val darkModeHelper: DarkModeHelper = DarkModeHelper(application.baseContext)

    fun setDarkMode() {
        darkModeHelper.applyDayNightMode()
    }
}

