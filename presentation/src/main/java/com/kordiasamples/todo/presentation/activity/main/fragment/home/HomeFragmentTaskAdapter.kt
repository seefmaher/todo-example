package com.kordiasamples.todo.presentation.activity.main.fragment.home

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.kordiasamples.todo.domain.model.TaskDTO
import com.kordiasamples.todo.presentation.R
import com.kordiasamples.todo.presentation.activity.main.fragment.home.HomeFragmentTaskAdapter.ViewHolder
import kotlinx.android.synthetic.main.item_task.view.*

class HomeFragmentTaskAdapter(private var dataSet: List<TaskDTO>?) : Adapter<ViewHolder>() {

    fun updateDataSet(newDataSet: List<TaskDTO>){
        dataSet = newDataSet
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardView.tv_task_title.text = dataSet?.get(position)?.taskTitle
        holder.cardView.rb_task_completed.isChecked = dataSet?.get(position)?.taskCompleted ?: false
        holder.cardView.setOnClickListener {
            Log.d(
                "taskitem",
                dataSet?.get(position)?.taskTitle + " clicked"
            )
        }
    }

    class ViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_task, parent, false) as CardView
        return ViewHolder(cardView)
    }

    override fun getItemCount(): Int = dataSet?.size!!
}