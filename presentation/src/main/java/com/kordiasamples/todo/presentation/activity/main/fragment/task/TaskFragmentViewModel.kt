package com.kordiasamples.todo.presentation.activity.main.fragment.task

import android.util.Log
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class TaskFragmentViewModel @Inject constructor(): ViewModel() {
    fun showLog() = Log.d("TaskFragmentViewModel", "Is working")
}