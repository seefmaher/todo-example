package com.kordiasamples.todo.presentation

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager

class DarkModeHelper(context: Context) {
    private val sharedPref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    fun applyDayNightMode() {
        when (sharedPref.getString("ui_mode", "0")) {
            "0" -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            "1" -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            "2" -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
    }
}