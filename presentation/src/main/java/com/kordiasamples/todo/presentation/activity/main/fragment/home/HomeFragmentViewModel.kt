package com.kordiasamples.todo.presentation.activity.main.fragment.home

import android.util.Log
import androidx.lifecycle.*
import com.kordiasamples.todo.domain.model.TaskDTO
import com.kordiasamples.todo.domain.repository.TaskRepository
import com.kordiasamples.todo.domain.usecase.task.TaskUsecase
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject

class HomeFragmentViewModel @Inject constructor(var taskUsecase: TaskUsecase) : ViewModel() {
//    @Inject
//    @Named("homestr")
//    lateinit var str: String

//    var list = mutableListOf<TaskDTO>()

//    val tasksList = MutableLiveData<List<TaskDTO>>()

    fun addTask() {
//        val taskDTO = TaskDTO(0, "1", "1", false)
        var i = 0
        Executors.newSingleThreadExecutor().execute {
            Log.d("timestart", Date().time.toString())
            while (i < 10000) {
                taskUsecase.insertTask(TaskDTO(0, i.toString(), "1", false))

                i++
            }
            Log.d("timeend", Date().time.toString())
            Log.d("timedevider", "--------------------------------")
        }
//        list.add(taskDTO)
//        tasksList.value = list
//        tasksList.value = taskUsecase.getAllTasks().subscribeOn(Schedulers.io()).toObservable()


    }

    fun getAllTasks(): LiveData<List<TaskDTO>> =
        LiveDataReactiveStreams.fromPublisher(
            taskUsecase.getAllTasks()
                .subscribeOn(Schedulers.io())
        )

}
