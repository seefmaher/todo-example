package com.kordiasamples.todo.presentation.activity.main.fragment.task

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.kordiasamples.todo.presentation.R
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class TaskFragment: DaggerFragment() {

    @Inject
    lateinit var provider: ViewModelProvider.Factory

    val viewModel: TaskFragmentViewModel by lazy {
        ViewModelProvider(this, provider).get(TaskFragmentViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.showLog()
        return inflater.inflate(R.layout.fragment_task, container, false)
    }

}