package com.kordiasamples.todo.presentation.activity.login

import android.util.Log
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class LoginActivityViewModel @Inject constructor() : ViewModel() {
    fun showLog() =
        Log.d("LoginActivityViewModel", "Is working")

}