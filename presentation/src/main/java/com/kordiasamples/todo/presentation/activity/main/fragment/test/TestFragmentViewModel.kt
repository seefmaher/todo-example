package com.kordiasamples.todo.presentation.activity.main.fragment.test

import android.util.Log
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class TestFragmentViewModel @Inject constructor(): ViewModel() {
    fun showLog() = Log.d("TestFragmentViewModel", "Is working")
}